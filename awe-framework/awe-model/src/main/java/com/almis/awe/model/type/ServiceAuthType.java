package com.almis.awe.model.type;

public enum ServiceAuthType {
  /**
   * Basic authentication
   */
  BASIC
}
