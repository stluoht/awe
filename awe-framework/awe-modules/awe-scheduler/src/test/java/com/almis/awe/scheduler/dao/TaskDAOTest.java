package com.almis.awe.scheduler.dao;

import com.almis.awe.model.component.AweElements;
import com.almis.awe.model.dto.CellData;
import com.almis.awe.model.dto.DataList;
import com.almis.awe.model.dto.ServiceData;
import com.almis.awe.model.util.data.QueryUtil;
import com.almis.awe.scheduler.bean.calendar.Schedule;
import com.almis.awe.scheduler.bean.task.Task;
import com.almis.awe.scheduler.bean.task.TaskDependency;
import com.almis.awe.scheduler.bean.task.TaskExecution;
import com.almis.awe.scheduler.enums.TaskLaunchType;
import com.almis.awe.scheduler.enums.TaskStatus;
import com.almis.awe.scheduler.filechecker.FileChecker;
import com.almis.awe.service.MaintainService;
import com.almis.awe.service.QueryService;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.springframework.context.ApplicationContext;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.almis.awe.scheduler.constant.TaskConstants.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

/**
 * Class used for testing Task DAO class
 */
@Slf4j
@ExtendWith(MockitoExtension.class)
class TaskDAOTest {

  private TaskDAO taskDAO;

  @Mock
  private MaintainService maintainService;

  @Mock
  private QueryService queryService;

  @Mock
  private QueryUtil queryUtil;

  @Mock
  private Scheduler scheduler;

  @Mock
  private ApplicationContext context;

  @Mock
  private AweElements aweElements;

  @Mock
  private CalendarDAO calendarDAO;

  @Mock
  private ServerDAO serverDAO;

  @Mock
  private FileChecker fileChecker;

  /**
   * Initializes json mapper for tests
   */
  @BeforeEach
  void initBeans() {
    taskDAO = new TaskDAO(scheduler, 5, "", queryService, maintainService, queryUtil, calendarDAO, serverDAO, fileChecker);
    taskDAO.setApplicationContext(context);
  }

  /**
   * Test context loaded
   */
  @Test
  void contextLoads() {
    // Check that controller are active
    assertNotNull(taskDAO);
  }

  /**
   * Change task test
   */
  @Test
  void changeTask() throws Exception {
    // Mock and spy
    given(queryUtil.getParameters((String) isNull())).willReturn(JsonNodeFactory.instance.objectNode());
    TaskExecution execution = new TaskExecution();
    execution.setStatus(TaskStatus.JOB_OK.getValue());
    execution.setDescription("Allright");

    // Run method
    taskDAO.changeStatus(new Task(), execution, TaskStatus.JOB_WARNING, "Because reasons");

    // Assert
    assertSame(TaskStatus.JOB_WARNING.getValue(), execution.getStatus());
    assertSame("Because reasons", execution.getDescription());
    verify(maintainService, times(1)).launchPrivateMaintain(anyString(), any(ObjectNode.class));
  }

  /**
   * Pause task test
   */
  @Test
  void pauseTask() throws Exception {
    // Mock and spy
    Task task = new Task();
    task.setTaskId(1);
    task.setGroup("TASK_GROUP");
    given(scheduler.checkExists(any(TriggerKey.class))).willReturn(true);

    // Run method
    taskDAO.pauseTask(task);

    // Assert
    verify(scheduler, times(1)).pauseTrigger(any(TriggerKey.class));
  }

  /**
   * Pause task without trigger checked
   */
  @Test
  void pauseTaskNoTrigger() throws Exception {
    // Mock and spy
    Task task = new Task();
    task.setTaskId(1);
    task.setGroup("TASK_GROUP");
    given(scheduler.checkExists(any(TriggerKey.class))).willReturn(false);

    // Run method
    taskDAO.pauseTask(task);

    // Assert
    verify(scheduler, times(0)).pauseTrigger(any(TriggerKey.class));
  }

  /**
   * Resume task test
   */
  @Test
  void resumeTask() throws Exception {
    // Mock and spy
    Task task = new Task();
    task.setLaunchType(1);
    task.setTaskId(1);
    task.setGroup("TASK_GROUP");
    task.setJob(null);
    task.setTrigger(TriggerBuilder.newTrigger().build());
    given(scheduler.checkExists(any(TriggerKey.class))).willReturn(true);
    given(scheduler.getTrigger(any(TriggerKey.class))).willReturn(TriggerBuilder.newTrigger().build());

    // Run method
    taskDAO.resumeTask(task);

    // Assert
    verify(scheduler, times(1)).rescheduleJob(any(TriggerKey.class), any(Trigger.class));
  }

  /**
   * Resume new task test
   */
  @Test
  void resumeNewTask() throws Exception {
    // Mock and spy
    Task task = new Task();
    task.setLaunchType(1);
    task.setTaskId(1);
    task.setGroup("TASK_GROUP");
    task.setJob(null);
    task.setTrigger(TriggerBuilder.newTrigger().build());

    // Run method
    taskDAO.resumeTask(task);

    // Assert
    verify(scheduler, times(1)).scheduleJob(eq(null), any(Trigger.class));
  }

  /**
   * Resume manual task test
   */
  @Test
  void resumeManualTask() throws Exception {
    // Mock and spy
    Task task = new Task();
    task.setLaunchType(0);
    task.setTaskId(1);
    task.setGroup("TASK_GROUP");
    task.setJob(null);
    task.setTrigger(TriggerBuilder.newTrigger().build());

    // Run method
    taskDAO.resumeTask(task);

    // Assert
    verify(scheduler, times(0)).scheduleJob(eq(null), any(Trigger.class));
  }

  /**
   * Get task execution from trigger
   */
  @Test
  void getTaskExecutionFromTrigger() {
    // Mock and spy
    Trigger trigger = TriggerBuilder.newTrigger().withIdentity("1-121", "TASK_GROUP").build();

    // Run method
    TaskExecution execution = taskDAO.getTaskExecution(trigger);

    // Assert
    assertSame(1, execution.getTaskId());
    assertSame(121, execution.getExecutionId());
    assertSame("TASK_GROUP", execution.getGroupId());
  }

  /**
   * Check task finish ok
   */
  @Test
  void checkTaskFinishOk() throws Exception {
    given(queryUtil.getParameters(any(), any(), any())).willReturn(JsonNodeFactory.instance.objectNode());
    DataList taskDataList = getTaskDataList(true);
    Task task = mockTask();
    given(queryService.launchPrivateQuery(anyString(), any(ObjectNode.class))).willReturn(
      new ServiceData().setDataList(taskDataList));
    TaskExecution parentExecution = new TaskExecution().setTaskId(2).setExecutionId(11).setStatus(TaskStatus.JOB_OK.getValue());
    TaskExecution execution = new TaskExecution().setTaskId(1).setExecutionId(12).setStatus(TaskStatus.JOB_OK.getValue()).setParentExecution(parentExecution);

    // Finish task
    taskDAO.onFinishTask(task, execution);

    // Assert
    assertSame(TaskStatus.JOB_OK.getValue(), parentExecution.getStatus());
  }

  /**
   * Check task finish error
   */
  @Test
  void checkTaskFinishError() throws Exception {
    doReturn(aweElements).when(context).getBean(any(Class.class));
    given(aweElements.getLocaleWithLanguage(anyString(), eq(null), any(Object[].class))).willReturn("LOCALE");
    given(queryUtil.getParameters(any(), any(), any())).willReturn(JsonNodeFactory.instance.objectNode());
    given(queryUtil.getParameters((String) isNull())).willReturn(JsonNodeFactory.instance.objectNode());

    DataList taskDataList = getTaskDataList(true);
    Task task = mockTask();
    given(queryService.launchPrivateQuery(anyString(), any(ObjectNode.class))).willReturn(
      new ServiceData().setDataList(taskDataList));
    TaskExecution parentExecution = new TaskExecution().setTaskId(2).setExecutionId(11).setStatus(TaskStatus.JOB_OK.getValue());
    TaskExecution execution = new TaskExecution().setTaskId(1).setExecutionId(12).setStatus(TaskStatus.JOB_ERROR.getValue()).setParentExecution(parentExecution);

    // Finish task
    taskDAO.onFinishTask(task, execution);

    // Assert
    assertSame(TaskStatus.JOB_WARNING.getValue(), parentExecution.getStatus());
  }

  /**
   * Check task finish error
   */
  @Test
  void checkTaskFinishErrorParentOk() throws Exception {
    given(queryUtil.getParameters(any(), any(), any())).willReturn(JsonNodeFactory.instance.objectNode());
    DataList taskDataList = getTaskDataList(false);
    Task task = mockTask(false);
    given(queryService.launchPrivateQuery(anyString(), any(ObjectNode.class))).willReturn(
      new ServiceData().setDataList(taskDataList));

    TaskExecution parentExecution = new TaskExecution().setTaskId(2).setExecutionId(11).setStatus(TaskStatus.JOB_OK.getValue());
    TaskExecution execution = new TaskExecution().setTaskId(1).setExecutionId(12).setStatus(TaskStatus.JOB_ERROR.getValue()).setParentExecution(parentExecution);

    // Finish task
    taskDAO.onFinishTask(task, execution);

    // Assert
    assertSame(TaskStatus.JOB_OK.getValue(), parentExecution.getStatus());
  }

  /**
   * Check task finish error
   */
  @Test
  void checkTaskFinishWarning() throws Exception {
    given(queryUtil.getParameters(any(), any(), any())).willReturn(JsonNodeFactory.instance.objectNode());
    DataList taskDataList = getTaskDataList(true);
    Task task = mockTask();
    given(queryService.launchPrivateQuery(anyString(), any(ObjectNode.class))).willReturn(
      new ServiceData().setDataList(taskDataList));
    TaskExecution parentExecution = new TaskExecution().setTaskId(2).setExecutionId(11).setStatus(TaskStatus.JOB_OK.getValue());
    TaskExecution execution = new TaskExecution().setTaskId(1).setExecutionId(12).setStatus(TaskStatus.JOB_WARNING.getValue()).setParentExecution(parentExecution);

    // Finish task
    taskDAO.onFinishTask(task, execution);

    // Assert
    assertSame(TaskStatus.JOB_OK.getValue(), parentExecution.getStatus());
  }

  /**
   * Check task finish error
   */
  @Test
  void checkTaskFinishInterrupted() throws Exception {
    given(queryUtil.getParameters(any(), any(), any())).willReturn(JsonNodeFactory.instance.objectNode());
    DataList taskDataList = getTaskDataList(true);
    Task task = mockTask();
    given(queryService.launchPrivateQuery(anyString(), any(ObjectNode.class))).willReturn(
      new ServiceData().setDataList(taskDataList));

    TaskExecution parentExecution = new TaskExecution().setTaskId(2).setExecutionId(11).setStatus(TaskStatus.JOB_OK.getValue());
    TaskExecution execution = new TaskExecution().setTaskId(1).setExecutionId(12).setStatus(TaskStatus.JOB_INTERRUPTED.getValue()).setParentExecution(parentExecution);

    // Finish task
    taskDAO.onFinishTask(task, execution);

    // Assert
    assertSame(TaskStatus.JOB_OK.getValue(), parentExecution.getStatus());
  }

  /**
   * Check if task execution is allowed
   */
  @Test
  void isTaskExecutionAllowed() {
    Task task = mockTask();
    task.setLaunchType(TaskLaunchType.FILE_TRACKING.getValue());
    given(fileChecker.checkFile(task)).willReturn("File");

    // Finish task
    boolean allowed = taskDAO.isTaskExecutionAllowed(task);

    // Assert
    assertTrue(allowed);
  }

  /**
   * Test new task without execution
   */
  @Test
  void startTask() throws Exception {
    given(queryUtil.getParameters(any(), any(), any())).willReturn(JsonNodeFactory.instance.objectNode());
    given(queryUtil.getParameters((String) isNull())).willReturn(JsonNodeFactory.instance.objectNode());
    given(queryService.launchPrivateQuery(anyString(), any(ObjectNode.class))).willReturn(new ServiceData().setDataList(new DataList()));
    Task task = new Task();
    task.setTrigger(TriggerBuilder.newTrigger().withIdentity("1", "TEST_GROUP").build());
    task.setParentExecution(new TaskExecution());

    // Finish task
    TaskExecution execution = taskDAO.startTask(task);

    // Assert
    assertNull(execution);
  }

  /**
   * Load execution screen
   */
  @Test
  void loadExecutionScreen() throws Exception {
    doReturn(aweElements).when(context).getBean(any(Class.class));
    given(aweElements.getLocaleWithLanguage(anyString(), eq(null))).willReturn("LOCALE");
    given(queryUtil.getParameters(any(), any(), any())).willReturn(JsonNodeFactory.instance.objectNode());
    ObjectNode address = JsonNodeFactory.instance.objectNode();
    address.put("row", "1" + TASK_SEPARATOR + "1");
    DataList dataList = new DataList();
    Map<String, CellData> row = new HashMap<>();
    row.put(TASK_IDE, new CellData(1));
    row.put("executionId", new CellData(1));
    row.put("initialDate", new CellData(new Date()));
    row.put("status", new CellData(TaskStatus.JOB_INTERRUPTED.getValue()));
    dataList.addRow(row);
    given(queryService.launchPrivateQuery(anyString(), any(ObjectNode.class))).willReturn(new ServiceData().setDataList(dataList));
    given(queryService.findLabel(anyString(), anyString())).willReturn("Label");

    // Finish task
    ServiceData serviceData = taskDAO.loadExecutionScreen("lala", address);

    // Assert
    assertEquals(16, serviceData.getClientActionList().size());
  }

  /**
   * Reload execution screen
   */
  @Test
  void reloadExecutionScreen() throws Exception {
    doReturn(aweElements).when(context).getBean(any(Class.class));
    given(aweElements.getLocaleWithLanguage(anyString(), eq(null))).willReturn("LOCALE");
    given(queryUtil.getParameters(any(), any(), any())).willReturn(JsonNodeFactory.instance.objectNode());

    ObjectNode address = JsonNodeFactory.instance.objectNode();
    address.put("row", "1" + TASK_SEPARATOR + "1");
    DataList dataList = new DataList();
    Map<String, CellData> row = new HashMap<>();
    row.put(TASK_IDE, new CellData(1));
    row.put("executionId", new CellData(1));
    row.put("initialDate", new CellData(new Date()));
    row.put("endDate", new CellData(new Date()));
    row.put("status", new CellData(TaskStatus.JOB_WARNING.getValue()));
    row.put("executionTime", new CellData(1231));
    dataList.addRow(row);
    given(queryService.launchPrivateQuery(anyString(), any(ObjectNode.class))).willReturn(new ServiceData().setDataList(dataList));
    given(queryService.findLabel(anyString(), anyString())).willReturn("Label");

    // Finish task
    ServiceData serviceData = taskDAO.reloadExecutionScreen(1, 1);

    // Assert
    assertEquals(13, serviceData.getClientActionList().size());
  }

  /**
   * Mock task
   *
   * @return Task task
   */
  private Task mockTask() {
    return mockTask(true);
  }

  /**
   * Mock a task
   *
   * @return Task mocked
   */
  private Task mockTask(boolean setTaskOnWarning) {
    return new Task()
            .setTaskId(1)
            .setExecutionType(1)
            .setLaunchType(TaskLaunchType.SCHEDULED.getValue())
            .setLaunchDependenciesOnError(true)
            .setLaunchDependenciesOnWarning(true)
            .setSetTaskOnWarningIfDependencyError(setTaskOnWarning)
            .setSchedule(new Schedule().setRepeatNumber(2).setRepeatType(2))
            .setDependencyList(Collections.singletonList(new TaskDependency().setTaskId(1).setParentId(2)));
  }

  private DataList getTaskDataList(boolean setTaskOnWarning) {
    // Mock
    DataList taskDataList = new DataList();
    Map<String, CellData> row = new HashMap<>();
    row.put(TASK_LAUNCH_TYPE, new CellData(1));
    row.put(TASK_IDE, new CellData(2));
    row.put("repeatType", new CellData(2));
    row.put("repeatNumber", new CellData(2));
    row.put("executionType", new CellData(1));
    row.put("parentId", new CellData(1));
    row.put("launchDependenciesOnError", new CellData(true));
    row.put("launchDependenciesOnWarning", new CellData(true));
    row.put("setTaskOnWarningIfDependencyError", new CellData(setTaskOnWarning));
    row.put(FILE_PATH, new CellData("file/path"));
    row.put(UPDATE_DATE, new CellData(new Date()));
    taskDataList.addRow(row);

    return taskDataList;
  }
}
