package com.almis.awe.testing.constants;

public class TestingConstants {

  private TestingConstants() {
    // Private constructor
  }

  public static final String DAY = "day";
  public static final String MONTH = "month";
  public static final String YEAR = "year";
}
