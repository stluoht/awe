package com.almis.awe.testing.model.types;

/**
 * Recording save mode enum
 */
public enum RecordingSaveType {
  /**
   * Save all video of tests
   */
  ALL,
  /**
   * Save only test failed videos
   */
  FAILED
}
